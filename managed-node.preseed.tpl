#_preseed_V1

# Debian Managed Node Preseed

############################
## localechooser
############################

### Set en_US.UTF-8 system locale
d-i debian-installer/locale string en_US.UTF-8

### Also install de_DE.UTF-8
d-i localechooser/supported-locales multiselect	de_DE.UTF-8

### Continue the installation in the selected language if the
### translation of the installer is incomplete.
d-i localechooser/translation/warn-severe boolean true
d-i localechooser/translation/warn-light boolean true

############################
## keyboard-configuration
############################

### Select German keyboard layout
d-i keyboard-configuration/xkb-keymap select de

############################
## netcfg
############################

### Auto select primary network interface
d-i netcfg/choose_interface select auto

## Auto-configure networking
d-i netcfg/use_autoconfig boolean true

############################
## choose-mirror-bin
############################

### Manually select German http mirror
d-i mirror/protocol string http
d-i mirror/country string manual
d-i mirror/http/hostname string ftp.de.debian.org
d-i mirror/http/directory string /debian
d-i mirror/http/proxy string

### Install Debian stable
d-i mirror/suite string stable

############################
## user-setup
############################

### Skip creation of a root account (normal user account will be able to
### use sudo).
d-i passwd/root-login boolean false

### Create a normal user account.
d-i passwd/user-fullname string Cloud User
d-i passwd/username string cloud

### Set temporary password
d-i passwd/user-password password r00tme
d-i passwd/user-password-again password r00tme

############################
## clock-setup
############################

### Confirm the System is clock set to UTC
d-i clock-setup/utc boolean true

### Set the clock using NTP
d-i clock-setup/ntp boolean true

############################
## tzsetup
############################

### Set timezone to Berlin
d-i time/zone string Europe/Berlin

############################
## partman-auto
############################

### Use LVM method for partitioning
d-i partman-auto/method string lvm

### Use atomic partition scheme
d-i partman-auto/choose_recipe select atomic

############################
## partman-auto-lvm
############################

### Use maximum size of volume group to use for guided partitioning
d-i partman-auto-lvm/guided_size string max

############################
## partman-lvm
############################

### Remove existing logical volume data
d-i partman-lvm/device_remove_lvm boolean true

### Avoid warning if the disk contains an old LVM configuration
d-i partman-lvm/confirm boolean true
d-i partman-lvm/confirm_nooverwrite boolean true

############################
## partman-partitioning
############################

### Write a new empty partition table without confirmation
d-i partman-partitioning/confirm_write_new_label boolean true

############################
## partman-base
############################

### Make partman automatically partition without confirmation
d-i partman/choose_partition select finish
d-i partman/confirm boolean true
d-i partman/confirm_nooverwrite boolean true

############################
## tasksel
############################

### Install standard system
tasksel tasksel/first multiselect standard, ssh-server

############################
## grub-installer
############################

### Make grub install automatically to the boot record if no other operating
### system is detected on the machine.
d-i grub-installer/only_debian boolean true

### Make grub install automatically to the boot record, if it also finds some
### other OS
d-i grub-installer/with_other_os boolean true

### Install grub to the primary device
d-i grub-installer/bootdev string default

############################
## preseed-common
############################

### Shell commands executed just before installation finishes
### - Install MANAGED_NODE_AUTHORIZED_KEY
### - Enable passwordless sudo
### - Disable password authentication
d-i preseed/late_command string in-target mkdir -p /home/cloud/.ssh/ ; \
                                in-target chmod 700 /home/cloud/.ssh/ ; \
                                in-target /bin/sh -c 'echo "{{ MANAGED_NODE_AUTHORIZED_KEY }}" >> /home/cloud/.ssh/authorized_keys' ; \
                                in-target chown -R cloud:cloud /home/cloud/.ssh/ ; \
                                in-target /bin/sh -c 'echo "cloud ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers.d/cloud' ; \
                                in-target chmod 440 /etc/sudoers.d/cloud ; \
                                in-target sed 's/^#PasswordAuthentication .*/PasswordAuthentication no/' -i /etc/ssh/sshd_config ; \
                                in-target passwd -d cloud ;

############################
## finish-install
############################

### Avoid that last message about the install being complete.
d-i finish-install/reboot_in_progress note

############################
## rootskel
############################

### Make the installer shutdown when finished, but not reboot into the
### installed system.
d-i debian-installer/exit/halt boolean true

