# Debian Preseed Managed Node

Debian Installer preseed file for managed nodes.

- Install a minimal system with sshd and sets up a root account with no password and an `authorized_keys` file
- The resulting installation should then be further provisioned with the [Managed Node Ansible Role](https://gitlab.gwdg.de/medinfpub/mi-xnat/ansible-role-managed-node)

## Usage

### Make the preseed file

- Running `make` will create the file `managed-node.preseed`, with the root authorized key file set to the first key output by the `ssh-add -L` command
- You can optionally specify the root authorized key by passing it as an argument to the make command, eg:
```
make MANAGED_NODE_AUTHORIZED_KEY='ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDhE1Gn4rW90zhteaTUJc0ymg6KBHbDZGlgn9DnBwQprjwUgY+FtAVZoDJ3tFHahH9P5UAZvctEdyf1ttoCRw2npRCNctlbHtfyu6jZ4rxJtCiFiTCl1MrjbhDq4/NrocWvIfYrh/4v0aaH/XNj9QIf6K32qCciXmuCqbqL/2Qj/jxnlk6VRzMH9HrxzUJa2osymclPzZWfkdalUaLMbiFuCuxas2bDCWEHws14ydBmW2E1oeWd/Lgjj3myXv+sLO81SmvR16r3+MbNHM6JnjW5d9Dk2pNUVmtcfshhuIITrQMAJShy7/sjBedV4syk1P/0HclPbGAETlHmaWn7hL49WhQCt8XrBw2Y60b8lLG29jxcBkvVTdiJ6JT3TSHWnXgocpsbdWWLe+1sD+7eBhDjhk2dSlvxU98ep+g6oqxMVo6CjpXJhVw2K5HaJMCvNy1InixlORYwu/xfh2YzCFc3qQVR2IyT9yPuvrunU4relZteL1Zzoa+QquR8F4TAJFc='
```

### Using the preseed file

- Host the `managed-node.preseed` file on a webserver that can be accessed by the machine you installing Debian on
- Boot a standard [Debian netinstall CD](https://www.debian.org/CD/netinst/)
- When the graphical installer boot menu appears, press ESC
- Type `auto url=http://<YOUR_WEBSERVER_HOST>/path/to/managed-node.preseed`
- When the installation is finished manually reboot the machine
