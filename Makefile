MANAGED_NODE_AUTHORIZED_KEY ?= $(shell ssh-add -L | head -n1 | cut -d' ' -f 1-2)
WWWDIR ?= /var/www/html
WWWUSER ?= nobody
WWWGROUP ?= www-data

managed-node.preseed:
	sed 's|{{ MANAGED_NODE_AUTHORIZED_KEY }}|$(MANAGED_NODE_AUTHORIZED_KEY)|g' \
		managed-node.preseed.tpl > managed-node.preseed

.PHONY: clean
clean:
	rm -f managed-node.preseed

.PHONY: install
install:
	install -o $(WWWUSER) -g $(WWWGROUP) -m 644 managed-node.preseed $(WWWDIR)


.PHONY: uninstall
uninstall:
	rm -f $(WWWDIR)/managed-node.preseed

